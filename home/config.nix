{
  packageOverrides = pkgs: {
    nur = import
      (builtins.fetchTarball {
        url = "https://github.com/nix-community/NUR/archive/d0b6a5b2cd03f67be19939eff29c4485424ee941.tar.gz";
        sha256 = "0rfapvac0w2jgf1n3x3p86z4415wiaix66vmrfy85xy6bxjcnah3";
      })
      {
        inherit pkgs;
      };
  };
}
