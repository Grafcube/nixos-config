# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
# vim: ft=nix

{ config, pkgs, lib, ... }:

let
  hosts = builtins.replaceStrings [ "0.0.0.0 s.youtube.com" ] [ "#0.0.0.0 s.youtube.com" ]
    (builtins.readFile
      (pkgs.fetchurl
        {
          url = "https://raw.githubusercontent.com/StevenBlack/hosts/bbb72c6956fdc8839042d477ea53d7f402adfe81/hosts";
          sha256 = "09zj5hl2gf2hnnwwy81zkzc10ly8aphwfc0fxwrpl7ac6wf7jfci";
        }));
  unstable = import <nixos-unstable> { config = { allowUnfree = true; }; };
  nur = import
    (builtins.fetchTarball {
      url = "https://github.com/nix-community/NUR/archive/c4901dc7660f78543d5877e7ad0c35ac1e22b1a7.tar.gz";
      sha256 = "1az5l9wwmh096pxx96mk08wd7flg108p73v6dl0zny6rhjx0sdn8";
    })
    {
      inherit pkgs;
    };
in
{
  imports =
    [
      # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./host.nix
      ./users.nix
      nur.repos.ilya-fedin.modules.flatpak-fonts
      nur.repos.ilya-fedin.modules.flatpak-icons
    ];

  fileSystems = {
    "/".options = [ "autodefrag" "compress-force=zstd" "relatime" ];
    "/boot".options = [ "noatime" "fmask=0033" "dmask=0022" "iocharset=ascii" "shortname=mixed" "utf8" "errors=remount-ro" ];
    "/home".options = [ "autodefrag" "compress-force=zstd" "relatime" ];
    "/nix".options = [ "autodefrag" "compress-force=zstd" "noatime" ];
    "/var/cache".options = [ "autodefrag" "compress-force=zstd" "noatime" ];
    "/var/log".options = [ "autodefrag" "compress-force=zstd" "noatime" ];
    "/var/tmp".options = [ "autodefrag" "compress-force=zstd" "noatime" ];
  };

  hardware = {
    opengl.enable = true;
    pulseaudio.enable = false;
    enableAllFirmware = true;
  };

  # Use the systemd-boot EFI boot loader.
  boot = {
    tmpOnTmpfs = true;
    supportedFilesystems = [ "zfs" ];
    loader = {
      timeout = 1;
      systemd-boot = {
        enable = true;
        configurationLimit = 12;
        consoleMode = "max";
        editor = false;
      };
      efi.canTouchEfiVariables = true;
    };
    kernelParams = [
      "quiet"
      "splash"
      "add_efi_memmap"
      "audit=1"
      "vt.global_cursor_default=0"
      "usbcore.autosuspend=-1"
      "intel_iommu=on"
      "iommu=pt"
    ];
    initrd.systemd.enable = true;
    plymouth = {
      enable = true;
      theme = "breeze";
    };
  };

  networking = {
    hostId = "d17c0ed8";
    nameservers = [
      "9.9.9.11"
      "149.112.112.11"
      "2620:fe::11"
      "2620:fe::fe:11"
    ];
    networkmanager = {
      enable = true;
      #dns = "none";
      #insertNameservers = [
      #"9.9.9.11"
      #"149.112.112.11"
      #"2620:fe::11"
      #"2620:fe::fe:11"
      #];
    };
    extraHosts = hosts;
  };

  # Set your time zone.
  time.timeZone = "Asia/Kolkata";

  # Select internationalisation properties.
  i18n = {
    defaultLocale = "en_IN.UTF-8";
    inputMethod = {
      enabled = "fcitx5";
      fcitx5.addons = with pkgs; [
        fcitx5-configtool
        fcitx5-gtk
        fcitx5-hangul
        fcitx5-mozc
        libsForQt5.fcitx5-qt
      ];
    };
  };

  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  #   useXkbConfig = true; # use xkbOptions in tty.
  # };

  systemd = {
    extraConfig = ''
      DefaultTimeoutStartSec=10s
      DefaultTimeoutStopSec=10s
    '';
  };

  security = {
    sudo.enable = false;
    doas = {
      enable = true;
      extraRules = [{
        groups = [ "wheel" ];
        persist = true;
        setEnv = [
          "NIX_PATH"
          "EDITOR"
          "VISUAL"
          "PAGER"
          "WAYLAND_DISPLAY"
          "DISPLAY"
          "XDG_RUNTIME_DIR"
          "XDG_CURRENT_DESKTOP"
          "XDG_SESSION_TYPE"
          "https_proxy"
        ];
      }];
    };
    apparmor = {
      enable = true;
      enableCache = true;
      packages = [ pkgs.apparmor-profiles ];
    };
    audit.enable = true;
    auditd.enable = true;
  };

  # Enable services
  services = {
    xserver = {
      enable = true;
      displayManager.gdm = {
        enable = true;
        autoSuspend = false;
      };
      desktopManager.gnome.enable = true;
    };
    btrfs.autoScrub.enable = true;
    pipewire = {
      enable = true;
      audio.enable = true;
      wireplumber.enable = true;
      alsa.enable = true;
      jack.enable = true;
      pulse.enable = true;
    };
    # opensnitch
    flatpak.enable = true;
    packagekit.enable = true;
    gvfs.enable = true;
    pcscd.enable = true;
  };

  # Enable sound.
  sound.enable = true;

  environment = {
    shells = [ pkgs.bashInteractive pkgs.zsh ];

    # List packages installed in system profile. To search, run:
    systemPackages = (with pkgs; [
      unstable.adw-gtk3
      nur.repos.grafcube.antidot
      bat
      breeze-qt5
      btop
      chezmoi
      dash
      dex
      nur.repos.grafcube.doasedit
      easyeffects
      exa
      fd
      file
      fzf
      gnumake
      gparted
      gsound
      hdparm
      imagemagick
      jq
      killall
      libqalculate
      nautilus-open-any-terminal
      neofetch
      neovide
      unstable.neovim
      nix-index
      nixos-option
      libgda
      libnotify
      libsecret
      lz4
      papirus-icon-theme
      patchelf
      unstable.pinentry
      playerctl
      pmutils
      unstable.polychromatic
      pv
      qt5ct
      rclone
      ripgrep
      rmtrash
      rnix-lsp
      tmux
      trash-cli
      tree
      unstable.wezterm
      unstable.xplr
      unzip
      wl-clipboard
      xclip
      yt-dlp
      (pkgs.writeScriptBin "youtube-dl" ''exec yt-dlp "$@"'') # Link to youtube-dl
      zoxide

      gnomeExtensions.gsconnect

    ]) ++ (with pkgs.gnome; [
      gnome-tweaks
    ]);

    gnome.excludePackages = (with pkgs; [
      gnome-photos
      gnome-tour
      gnome-console
    ]) ++ (with pkgs.gnome; [
      cheese
      gedit
      epiphany
      geary
      gnome-music
      gnome-shell-extensions
      gnome-terminal
      evince
      gnome-characters
      totem
      tali
      iagno
      hitori
      atomix
    ]);
  };

  fonts = {
    enableDefaultFonts = true;
    fontDir.enable = true;
    fonts = with pkgs; [
      nur.repos.grafcube.lora-cyrillic
      liberation_ttf
      merriweather-sans
      noto-fonts
      noto-fonts-cjk
      twemoji-color-font
      (nerdfonts.override { fonts = [ "JetBrainsMono" ]; })
    ];
  };

  programs = {
    zsh = {
      enable = true;
      enableBashCompletion = true;
    };
    dconf.enable = true;
    gnupg.agent = {
      enable = true;
      enableBrowserSocket = true;
      enableSSHSupport = true;
    };
    git = {
      enable = true;
      lfs.enable = true;
      config = {
        credential.helper = "${pkgs.git.override { withLibsecret = true; }
        }/bin/git-credential-libsecret";
      };
    };
  };

  nix.extraOptions = ''
    experimental-features = nix-command flakes
  '';

  system = {
    autoUpgrade.enable = true;

    # Copy the NixOS configuration file and link it from the resulting system
    # (/run/current-system/configuration.nix). This is useful in case you
    # accidentally delete configuration.nix.
    copySystemConfiguration = true;

    # This value determines the NixOS release from which the default
    # settings for stateful data, like file locations and database versions
    # on your system were taken. It‘s perfectly fine and recommended to leave
    # this value at the release version of the first install of this system.
    # Before changing this value read the documentation for this option
    # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
    stateVersion = "22.05"; # Did you read the comment?
  };
}
