# vim: ft=nix

{ config, pkgs, ... }:

{
  networking.hostName = "vpcnix";

  # swapDevices =
  #   [{
  #     device = "/dev/disk/by-uuid/ad8331b4-7115-4781-b00b-e324fe2bbc26";
  #     encrypted = {
  #       enable = true;
  #       keyFile = "/mnt-root/etc/keys/swap.key"; # Yes, /mnt-root is correct.
  #       label = "swap"; # The name used with `cryptsetup` when unlocking the LUKS container. It can be whatever you want, ex "luksswap".
  #       blkDev = "/dev/disk/by-uuid/07dff021-d51a-4094-8f1b-06d8603cbc0e";
  #     };
  #   }];

  boot = {
    kernelPackages = pkgs.linuxPackages_zen;
    extraModulePackages = with config.boot.kernelPackages; [ nvidia_x11 ];
    initrd.kernelModules = [ "nvidia" ];
  };

  fileSystems = {
    "/".options = [ "ssd" ];
    "/home".options = [ "ssd" ];
    "/nix".options = [ "ssd" ];
    "/var/cache".options = [ "ssd" ];
    "/var/log".options = [ "ssd" ];
    "/var/tmp".options = [ "ssd" ];
  };

  hardware = {
    cpu.intel.updateMicrocode = true;
    nvidia.modesetting.enable = true;
    openrazer.enable = true;
  };

  services = {
    xserver.videoDrivers = [ "nvidia" ];
    openssh = {
      enable = true;
      forwardX11 = true;
      passwordAuthentication = false;
      permitRootLogin = "no";
      startWhenNeeded = true;
    };
  };

  nixpkgs.config.allowUnfree = true;
}
